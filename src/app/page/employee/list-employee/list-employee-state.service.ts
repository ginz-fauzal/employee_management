import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListEmployeeStateService {
  private tableState: { [key: string]: any } = {};

  setTableState(key: string, state: any) {
    this.tableState[key] = state;
  }

  getTableState(key: string): any {
    return this.tableState[key] || null;
  }
}