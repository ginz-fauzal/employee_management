import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';

import { Employee } from '../../../models/employee.model';
import { EmployeeService } from '../service-employee/employee.service';
import { SharedModule } from '../../../shared/shared.module';
import { ListEmployeeStateService } from './list-employee-state.service';

@Component({
  selector: 'app-list-employee',
  standalone: true,
  imports: [SharedModule],
  providers: [MessageService, ConfirmationService],
  templateUrl: './list-employee.component.html',
  styleUrl: './list-employee.component.css'
})
export class ListEmployeeComponent implements OnInit,OnDestroy {

  @ViewChild('dt1') dt1!: Table;
  employees: Employee[] = [];
  globalFilter: string='';
  first: number = 0;
  rows: number = 10;
  
  constructor(
    private employeeService: EmployeeService,
    private messageService: MessageService, 
    private confirmationService: ConfirmationService,
    private listEmployeeStateService: ListEmployeeStateService,
    private router:Router
  ) {
    this.employees = this.employeeService.getData();
  }

  ngOnInit() {
    const savedTableState = this.listEmployeeStateService.getTableState('employeesTable');
    setTimeout(() => {
      if (savedTableState) {
        this.globalFilter = savedTableState.globalFilter;
        this.first = savedTableState.first;
        this.rows = savedTableState.rows;
        this.dt1.filterGlobal(this.globalFilter, 'contains');
      }
    }); 
  }

  ngOnDestroy() {
    const currentTableState = {
      globalFilter: this.globalFilter,
      first: this.dt1.first,
      rows: this.dt1.rows
    };
    this.listEmployeeStateService.setTableState('employeesTable', currentTableState);
  }

  deleteEmployee(employee: Employee) {
    console.log("tes")
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete ' + employee.firstName + '?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.employees = this.employees.filter((val) => val.username !== employee.username);
            this.messageService.add({ severity: 'error', summary: 'Successful', detail: 'Product Deleted', life: 3000 });
        }
    });
  }

  editEmployee(){
    this.messageService.add({ severity: 'warn', summary: 'Successful', detail: 'Product Updated', life: 3000 });
  }

  detailEmployee(username: string): void {
    const currentTableState = {
      globalFilter: this.globalFilter,
      first: this.dt1.first,
      rows: this.dt1.rows
    };
    this.listEmployeeStateService.setTableState('employeesTable', currentTableState);
    this.router.navigate(['/employee/detail', username]);
  }
}
